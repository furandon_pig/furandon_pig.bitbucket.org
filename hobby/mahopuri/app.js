APP = (function(){
    var text1 = document.getElementById('text1');
    var result = document.getElementById('result');
     
    init = function() {
        inputKey();
        text1.focus();
    }
  
    inputKey = function() {
        result.innerHTML = text1.value
            .replace(/ /g, '&nbsp;').replace(/\t/g, '    ')
            .replace(/</g, '&lt;').replace(/>/, '&gt;')
            .replace(/\n/g, '<br>');
    }
 
    insertSampleText = function(sampleNum) {
        if (sampleNum == undefined) {
            return;
        }
        
        var text = '';
        switch (sampleNum) {
            case 1:
                text = '魔法つかいprecure!';
                break;
            case 2:
                text = 'MAHO GIRLS PRECURE!';
                break;
            case 3:
                text = 'MIRACLE MAGICAL';
                break;
            case 4:
                text = 'OTAYORI!!';
                break;
            case 5:
                text = 'MAHOCA';
                break;
            case 6:
                text = 'MAHO SCHOOL';
                break;
            case 7:
                text = 'TO THE SKY';
                break;
            case 8:
                text += 'HANABIRA BLUE FUSHIGI FLOWER BLUE ' + '\n';
                text += 'MAHOKAI FUSHIGI HANABIRA FLOWER ' + '\n';
                text += 'MAHOKAI BLUE KIREI BLUE';
                break;
            case 9:
                text += 'I will give you supplementary lessons.' + '\n';
                text += 'total 6 times. If you fail the test even' + '\n';
                text += 'you stay in the same class for another';
                break;
            default:
                // do nothing
        }
        
        if (text1.value == '') {
            text1.value = text1.value + text;
        } else {
            text1.value = text1.value + '\n' + text;
        }

        inputKey();
    }

    return {
        "init":init,
        "inputKey":inputKey,
        "insertSampleText":insertSampleText,
    }
})();
