/* myapp.js */

var app = angular.module('MyApp', []);
app.controller('MyAppCtrl', ['$scope',
  function($scope) {

    var id = 0;

    /* コントローラの初期化 */
    var MY_APP_KEY        = "OfficeUI_JSON_ExportSample";
    var MY_APP_KEY_CONFIG = "OfficeUI_JSON_ExportSampleConfig";

    $scope.useRandomMemo = true;

    alertInfo = function(msg) {
      $scope.alertColor = "green";
      $scope.alertIcon  = "infoCircle";
      $scope.alertMsg   = msg;
    }

    alertWarn = function(msg) {
      $scope.alertColor = "yellow";
      $scope.alertIcon  = "alert";
      $scope.alertMsg   = msg;
    }

    alertErr = function(msg) {
      $scope.alertColor = "red";
      $scope.alertIcon  = "alert";
      $scope.alertMsg   = msg;
    }

    var defaultMsg = "JSONデータのエクスポート・インポートを行うサンプルプログラムです。";
    var appMsg = defaultMsg;
    alertInfo(appMsg);

    var randomMemos = [
      {
        memo : "Office UI Fabric https://github.com/OfficeDev/Office-UI-Fabric",
      }, {
        memo : "AngularJS https://angularjs.org/",
      }, {
        memo : "死神はりんごしかたべない",
      }, {
        memo : "「メメクラゲ」は「××クラゲ」の誤植",
      }, {
        memo : "ツェラーの公式を使えば日付から直接曜日の計算ができる",
      }, {
        memo : "AIDMA(アイドマ)の法則、Attention(注意),Interest(関心),Desire(欲求),Memory(記憶),Action(行動)",
      }, {
        memo : "モノを食べる時は誰にも邪魔されず、自由で、何というか救われていなきゃダメ",
      }, {
        memo : "ジェット(のシューマイ)を購入し、あとは向こうで水割りセットを買えば万全",
      }, {
        memo : "煮込み雑炊とお雑煮は冬場だけのメニュー",
      }, {
        memo : "持ち帰り！そういうのもある",
      }, {
        memo : "シャボテンは人の来ないそれは淋しい砂漠に生えている",
      }, {
        memo : "ソースの味は男のコ",
      }, {
        memo : "麦飯に醤油をかけて食うとうまい。ソースもまたオツ",
      }
    ];

    getRandomMemo = function() {
      var len = randomMemos.length;
      return randomMemos[parseInt(Math.random() * len)];
    }

    /* local storageからデータを読み込む */
    loadData = function() {
      var json_str = localStorage.getItem(MY_APP_KEY);
      if (json_str != null) {
        try {
          var items = JSON.parse(json_str);
          if (items.length == 0) {
            items = generateSampleData();
          } else {
            $scope.items = items;
          }
        } catch(e) {
          console.log('-=> JSON parse error!');
          $scope.items = generateSampleData();
        }

        // id値更新
        //var len = $scope.items.length;
        //id = $scope.items[len - 1].id + 1;
      } else {
        $scope.items = generateSampleData();
      }
    }

    generateSampleData = function() {
      return [
        {
          id     : id++,
          memo   : "Office UI Fabric",
          _show  : true
        }, {
          id     : id++,
          memo   : "AngularJS",
          _show  : true
        }
      ];
    }

    $scope.add = function() {
      if ($scope.useRandomMemo == true) {
        var obj = getRandomMemo();
        $scope.items.unshift({
          id     : id++,
          memo   : obj.memo,
          _show  : true
        });
      } else {
        $scope.items.unshift({
          id     : id++,
          memo   : "",
          _show  : true
        });
      }
    }

    $scope.add2 = function(index) {
      if (index == 0) {
        $scope.add();
      }
    }

    /* 1番目のメモのアイコンはメモ追加用アイコンにする */
    $scope.getNoteIcon = function(index) {
      if (index == 0) {
        return "circlePlus";
      } else {
        return "note";
      }
    }

    $scope.remove = function(item) {
      if ($scope.isConfirmWhenRemove == true) {
        $scope.showConfirm(
            "メモの削除",
            "メモ「" + item.memo + "」を削除しますか？",
            function() { removeItem(item) }
        );
      } else {
        removeItem(item)
      }
    }

    removeItem = function(item) {
      for (var i in $scope.items) {
        if ($scope.items[i].id == item.id) {
          $scope.items.splice(i, 1);
          $scope.search();
          return;
        }
      }
    }

    $scope.save = function() {
      try {
        saveData();
        alertInfo("データを保存しました。");
      } catch(e) {
        var msg =
          "データ保存時にエラーが発生しました。\n" +
          'local storageのデータkeyは"' + MY_APP_KEY + '"です。';
        alertErr(msg);
      }
    }

    $scope.import = function() {
      showImport();
    }

    $scope.export = function() {
      showExport();
    }

    /*
     * ダイアログボックス用のテキストを初期化
     */
    $scope.disableLocalStorage = false;
    $scope.storageClearInfo = "保存してあるデータを全て削除します。";

    $scope.clearLocalStorageData = function() {
      $scope.showConfirm(
          "データのクリア",
          "保存してあるデータをクリアしますか？",
          function() { clearLocalStorageData2() }
      );
    }

    clearLocalStorageData2 = function() {
      localStorage.removeItem(MY_APP_KEY);
      localStorage.removeItem(MY_APP_KEY_CONFIG);

      var len = $scope.items.length;
      $scope.items.splice(0, len);

      $scope.disableLocalStorage = true;
      //$scope.storageClearInfo = "データの保存を有効化する場合は、Webページを再読み込みしてください。";
      $scope.storageClearInfo = "local storageのデータ(メモ、設定データ)を削除しました。";
    }

    getJSONString = function() {
      var objs = [];

      // 必要な項目のみを抜き出す
      for (var i in $scope.items) {
        var obj = {
          memo   : $scope.items[i].memo
        };
        objs.push(obj);
      }

      /*
       * 人間が視認しやすい形のJSONテキストを生成する
       *   - keyとvalueの間のコロン「:」の前後には空白を入れない
       *   - Objectの最後のvalueの末尾にはカンマ「,」を入れない
       *   - 数値、論理値(Boolean)はクォート「"」で挟まない
       */
      var esc_quote = new RegExp('"', 'g');

      var is_first_bracket = true;
      var json_str = "[\n";
      for (var i in objs) {
        var tmp = '';
        if (is_first_bracket == true) {
          tmp = '  {' + "\n";
          is_first_bracket = false;
        }

        tmp += '    "memo":' + '"' + objs[i].memo.replace(esc_quote, '\\"') + '"' + "\n";

        if (i == (objs.length - 1)) {
          tmp += '  }';
        } else {
          tmp += '  }, {';
        }

        json_str = json_str + tmp + "\n";
      }
      json_str += ']';

      return json_str;
    }

    saveData = function() {
      localStorage.setItem(MY_APP_KEY, getJSONString());
    }

    $scope.doImport = function() {
      if ($scope.jsonData == "") { return; }

      try {
        var objs = JSON.parse($scope.jsonData);
      } catch(e) {
        $scope.importErrorColor = "red";
        $scope.importErrorIcon  = "alert";
        $scope.importErrorMsg   = "インポートエラーが発生しました。";
        return;
      }

      var skippedItem = 0;
      for (var i in objs) {
        if (objs[i].memo != undefined) {
          var obj = {
            id   : id++,
            memo : objs[i].memo,
            _show : true
          };
          // import時はunshift()ではなくpush()でデータを追加する
          $scope.items.push(obj);
        } else {
          skippedItem++;
        }
      }

      $scope.importErrorIcon  = "infoCircle";
      if (skippedItem > 0) {
        $scope.importErrorColor = "yellow";
        $scope.importErrorMsg   = "インポートされなかったデータが" + skippedItem + "件あります。";
      } else {
        $scope.importErrorColor = "green";
        $scope.importErrorMsg   = "データをインポートしました。";

        $scope.jsonData = "";
      }
    }

    /* 検索 */
    // 正規表現文字をエスケープした文字列を返す
    function escape_regexp_keyword(str) {
      var chars = [
        '\\', '*', '+', '.', '?', '{', '}', '(', ')',
        '[', ']', '^', '$', '-', '|', '/'
      ];

      escaped_str = "";
      for (var i = 0; i < str.length; i++) {
        var is_escaped = false;
        for (var j in chars) {
          if (str.charAt(i) == chars[j]) {
            escaped_str += '\\' + str.charAt(i);
            is_escaped = true;
            break;
          }
        }
        if (is_escaped == false) {
          escaped_str += str.charAt(i);
        }
      }

      return escaped_str;
    }

    // 検索処理を実行する
    $scope.search = function() {
      if ($scope.keyword == "" || $scope.keyword == undefined) {
        for (var i in $scope.items) {
          $scope.items[i]._show = true;
        }
        alertInfo(defaultMsg);
      } else {
        var keyword_regex = new RegExp(escape_regexp_keyword($scope.keyword), "i");

        var is_match = false;
        var match_count = 0;
        for (var i in $scope.items) {
          if ($scope.items[i].memo.match(keyword_regex) != null) {
            is_match = true;
            match_count++;
            $scope.items[i]._show = true;
          } else {
            $scope.items[i]._show = false;
          }
        }

        if (is_match == false) {
          alertWarn("キーワードにマッチするメモはありません。");
        } else {
          alertInfo(match_count + "件のメモがマッチしました。");
        }
      }
    }

    /*
     * 「設定」ダイアログ用
     */
    $scope.closeConfig = function() {
      if ($scope.disableLocalStorage == false) {
        var obj = {
          cfgSearchFs   : $scope.cfgSearchFs,
          cfgSearchNs   : $scope.cfgSearchNs,
          cfgSearchMemo : $scope.cfgSearchMemo,
          cfgHideSample : $scope.cfgHideSample,
        };
        localStorage.setItem(MY_APP_KEY_CONFIG, JSON.stringify(obj));
      }

      $scope.showFlgConfigDlg = "hidden";
    };

    $scope.showConfig  = function() {
      $scope.showFlgConfigDlg = "visible";
    }

    $scope.dismissConfig = function() {
      $scope.showFlgConfigDlg = "hidden";
    }

    /*
     * 「エクスポート」「インポート」ダイアログ用
     */
    showExport = function() {
      $scope.fileDialogInfo = "エクスポート";
      $scope.fileDialogIcon = "upload";
      $scope.isExport = true;

      $scope.jsonData = getJSONString();

      $scope.showFlgExportDlg = "visible";
    }

    showImport = function() {
      $scope.fileDialogInfo = "インポート";
      $scope.fileDialogIcon = "download";
      $scope.isExport = false;

      $scope.importErrorColor = "green";
      $scope.importErrorIcon  = "infoCircle";
      $scope.importErrorMsg   = "インポートするデータの形式はJSONです。";

      $scope.jsonData = "";

      $scope.showFlgImportDlg = "visible";
    }

    $scope.closeExport = function() { $scope.showFlgExportDlg = "hidden"; }
    $scope.closeImport = function() { $scope.showFlgImportDlg = "hidden"; }

    /*
     * 「About...」ダイアログ用
     */
    $scope.showAbout  = function() { $scope.showFlgAboutDlg = "visible"; }
    $scope.closeAbout = function() { $scope.showFlgAboutDlg = "hidden";  }

    /*
     * 「Confirm」ダイアログ用
     */
    $scope.showConfirm = function(title, msg, ok_func, no_func) {
      $scope.confirmTitle = title;
      $scope.confirmMsg   = msg;

      $scope.confirmTrueFunc  = ok_func;
      $scope.confirmFalseFunc = no_func;

      $scope.showFlgConfirmDlg = "visible";

      return false;
    }

    $scope.closeConfirmYes = function() {
      $scope.showFlgConfirmDlg = "hidden";
      if ($scope.confirmTrueFunc != undefined) {
        $scope.confirmTrueFunc();
      }
      return true;
    }

    $scope.closeConfirmNo = function() {
      $scope.showFlgConfirmDlg = "hidden";
      if ($scope.confirmFalseFunc != undefined) {
        $scope.confirmFalseFunc();
      }
      return true;
    }

    /*
     * ページ読み込み時にダイアログを全て閉じる
     */
    $scope.dismissConfig();
    $scope.closeExport();
    $scope.closeImport();
    $scope.closeAbout();
    $scope.closeConfirmNo();

    // 「メモの削除時に確認する」チェックボックス用
    $scope.isConfirmWhenRemove = true;

    loadData();
    for (var i in $scope.items) {
      $scope.items[i]._show = true;
    }
  }
]);

