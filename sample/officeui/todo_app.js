/* todo_app.js */

var app = angular.module('ToDoApp', []);

app.controller('ToDoAppCtrl', ['$scope',
  function($scope) {

    // localStorage保存時のkey
    var MY_APP_KEY = 'OfficeUI_ToDoApp';

    // データの初期化
    $scope.dlgToDo   = "hidden";
    $scope.dlgConfig = "hidden";
    $scope.dlgAbout  = "hidden";

    // ToDoの削除時に確認するかどうか
    $scope.isConfirmRemove = true;

    // 「完了したToDoも表示する」チェックボックス用
    $scope.showDoneTask = true;

    // 優先度の初期値(優先度＝中)
    $scope.prio = 1;

    $scope.disableLocalStorage == false;
    $scope.storage_clear_info = "保存してあるToDoデータを削除します。";

    var is_newTask = false;
    var editingIndex  = -1;
    var id = 0;

    var local_todos_str = localStorage.getItem(MY_APP_KEY);
    if (local_todos_str != null) {
      $scope.todos = JSON.parse(local_todos_str);
      if ($scope.todos.length != 0) {
        id = $scope.todos[0].id + 1;
      }
      for (var i in $scope.todos) {
        if ($scope.showDoneTask == true) {
          $scope.todos[i].showToDo = true;
        } else {
          $scope.todos[i].showToDo = !$scope.todos[i].done;
        }
      }
    } else {
      $scope.todos = [
        {
          id   : id++,
          task : "クマの本名を暗記する",
          prio : 0,
          memo : "クラウス・ヘンリック・ポンシェッター・フォン・ボーゲル・シュトローベル・キャッシュ・デ・ラ・マネッチャ三世",
          done : false,
          showToDo : true,
          sample : true,
        }, {
          id   : id++,
          task : "三位一体で夢のドアを開ける",
          prio : 0,
          memo : "CHANGE! MY WORLD",
          done : false,
          showToDo : true,
          sample : true,
        }, {
          id   : id++,
          task : "悲しい時こそ涙の声を聞く",
          prio : 0,
          memo : "HAPPYぱLUCKY",
          done : false,
          showToDo : true,
          sample : true,
        }, {
          id   : id++,
          task : "虹色に輝く夢を見つけに行く",
          prio : 1,
          memo : "ドリームパレード",
          done : false,
          showToDo : true,
          sample : true,
        }, {
          id   : id++,
          task : "定石を奇跡に変える",
          prio : 1,
          memo : "CHANGE! MY WORLD",
          done : false,
          showToDo : true,
          sample : true,
        }, {
          id   : id++,
          task : "明日を待ちわびる(寒い夜だから)",
          prio : 1,
          memo : "「寒い夜だから…」(1stシーズン第24話での挿入歌)",
          done : false,
          showToDo : true,
          sample : true,
        }, {
          id   : id++,
          task : "完璧じゃないけど完璧目指す",
          prio : 2,
          memo : "HAPPYぱLUCKY",
          done : false,
          showToDo : true,
          sample : true,
        }, {
          id   : id++,
          task : "感覚を無重力にする",
          prio : 0,
          memo : "太陽のflare sherbet",
          done : false,
          showToDo : true,
          sample : true,
        }, {
          id   : id++,
          task : "放映時間変更に対応する",
          prio : 0,
          memo : "10/5から18:30の放映",
          done : false,
          showToDo : true,
          sample : true,
        }
      ];
    }

    // 「サンプルToDoデータを削除する」ボタンの表示・非表示判断
    $scope.showRemoveAllSample = true;
    for (var i in $scope.todos) {
      if ($scope.todos[i].sample == true) {
        $scope.showRemoveAllSample = false;
        break;
      }
    }

    /* idから対応するToDoデータの配列インデックス値を取得する */
    lookupIndex = function(id) {
      for (var i in $scope.todos) {
        if ($scope.todos[i].id == id) {
          return i;
        }
      }
    }

    $scope.check = function(id) { 
      $scope.todos[id].done = ! $scope.todos[id].done;
      if ($scope.showDoneTask == true) {
        for (i in $scope.todos) {
          $scope.todos[id].showToDo = true;
        }
      } else {
        for (i in $scope.todos) {
          $scope.todos[id].showToDo = !$scope.todos[id].done;
        }
      }
      syncLocalStorage();
    };

    $scope.shotenMemo = function(id) {
      if ($scope.todos[id].memo == undefined) {
        return "";
      } else {
        if ($scope.todos[id].memo.length < 9) {
          return $scope.todos[id].memo;
        } else {
          return $scope.todos[id].memo.substr(0, 9) + "...";
        }
      }
    };

    $scope.getPriorityColor = function(id) {
      return ['ms-fontColor-green', 'ms-fontColor-black', 'ms-fontColor-red'][$scope.todos[id].prio];
    };

    $scope.getPriorityChar = function(task) {
      var index = lookupIndex(task.id);
      return ['低', '中', '高'][$scope.todos[index].prio];
    };

    /* 「ToDoの追加」ダイアログ用 */
    $scope.open = function() {
      is_newTask = true;
      $scope.openType = "追加";
      $scope.dlgIcon = "new";

      $scope.task = "";
      $scope.prio = 1;
      $scope.memo = "";

      $scope.dlgToDo = "visible";
    };

    $scope.check = function(id) { 
      $scope.todos[id].done = ! $scope.todos[id].done;
      if ($scope.showDoneTask == true) {
        $scope.todos[id].showToDo = true;
      } else {
        $scope.todos[id].showToDo = !$scope.todos[id].done;
      }
      syncLocalStorage();
    };

    $scope.getPriorityColor = function(todo) {
      if (todo == undefined) return;
      var index = [$scope.todos[lookupIndex(todo.id)].prio];
      return ['ms-fontColor-green', 'ms-fontColor-black', 'ms-fontColor-red'][index];
    };

    $scope.increasePriority = function(task) {
      var index = lookupIndex(task.id);

      var new_prio = ++$scope.todos[index].prio;
      if (new_prio > 2) {
        $scope.todos[index].prio = 2;
      } else {
        $scope.todos[index].prio = new_prio;
      }
      syncLocalStorage();
    };

    $scope.decreasePriority = function(task) {
      var index = lookupIndex(task.id);

      var new_prio = --$scope.todos[index].prio;
      if (new_prio < 0) {
        $scope.todos[index].prio = 0;
      } else {
        $scope.todos[index].prio = new_prio;
      }
      syncLocalStorage();
    };

    $scope.done = function(todo) {
      for (var i in $scope.todos) {
        if ($scope.showDoneTask == true) {
          $scope.todos[i].showToDo = true;
        } else {
          $scope.todos[i].showToDo = !$scope.todos[i].done;
        }
      }
      syncLocalStorage();
    }

    /* ToDoの削除 */
    $scope.remove = function(todo) {
      var index = lookupIndex(todo.id);

      if ($scope.isConfirmRemove == true) {
        $scope.showConfirm(
            "ToDoの削除",
            "ToDo「" + $scope.todos[index].task + "」を削除しますか？",
            function() { removeItem(index); }
        );
      } else {
        removeItem(index);
      }
    }

    removeItem = function(index) {
      $scope.todos.splice(index, 1);
      syncLocalStorage();
    }

    /*
     * 「エクスポート」「インポート」ダイアログ用
     */
    $scope.showExport = function() {
      $scope.fileDialogInfo = "エクスポート";
      $scope.fileDialogIcon = "upload";
      $scope.isExport = true;

      $scope.jsonData = getJSONString();

      $scope.showFlgExportDlg = "visible";
    }

    $scope.showImport = function() {
      $scope.fileDialogInfo = "インポート";
      $scope.fileDialogIcon = "download";
      $scope.isExport = false;

      $scope.importErrorColor = "green";
      $scope.importErrorIcon  = "infoCircle";
      $scope.importErrorMsg   = "インポートするデータの形式はJSONです。";

      $scope.jsonData = "";

      $scope.showFlgImportDlg = "visible";
    }

    $scope.closeExport = function() { $scope.showFlgExportDlg = "hidden"; }
    $scope.closeImport = function() { $scope.showFlgImportDlg = "hidden"; }

    getJSONString = function() {
      var objs = [];

      // 必要な項目のみを抜き出す
      for (var i in $scope.todos) {
        var obj = {
          task : $scope.todos[i].task,
          prio : $scope.todos[i].prio,
          memo : $scope.todos[i].memo,
          done : $scope.todos[i].done,
        };
        objs.push(obj);
      }

      /*
       * 人間が視認しやすい形のJSONテキストを生成する
       *   - keyとvalueの間のコロン「:」の前後には空白を入れない
       *   - Objectの最後のvalueの末尾にはカンマ「,」を入れない
       *   - 数値、論理値(Boolean)はクォート「"」で挟まない
       */
      var esc_quote   = new RegExp('"',  'g');
      var esc_newline = new RegExp("\n", 'g');

      var is_first_bracket = true;
      var json_str = "[\n";
      for (var i in objs) {
        var tmp = '';
        if (is_first_bracket == true) {
          tmp = '  {' + "\n";
          is_first_bracket = false;
        }

        var _task = objs[i].task.replace(esc_quote, '\\"').replace(esc_newline, '__BR__');
        var _memo = objs[i].memo.replace(esc_quote, '\\"').replace(esc_newline, '__BR__');

        tmp += '    "task":' + '"' + _task + '"' + ",\n";
        tmp += '    "prio":' + objs[i].prio + ",\n";
        tmp += '    "memo":' + '"' + _memo + '"' + ",\n";
        tmp += '    "done":' + objs[i].done + "\n";

        if (i == (objs.length - 1)) {
          tmp += '  }';
        } else {
          tmp += '  }, {';
        }

        json_str = json_str + tmp + "\n";
      }
      json_str += ']';

      return json_str;
    }

    $scope.doImport = function() {
      if ($scope.jsonData == "") { return; }

      var esc_newline = new RegExp('__BR__', 'g');

      try {
        var objs = JSON.parse($scope.jsonData);
      } catch(e) {
        $scope.importErrorColor = "red";
        $scope.importErrorIcon  = "alert";
        $scope.importErrorMsg   = "インポートエラーが発生しました。";
        return;
      }

      var skippedItem = 0;
      for (var i in objs) {
        if (objs[i].task != undefined) {
          var obj = {
            id   : id++,
            task: objs[i].task,
            prio: objs[i].prio,
            memo: objs[i].memo.replace(esc_newline, "\n"),
            done: objs[i].done,
            showToDo : true
          };
          // import時はunshift()ではなくpush()でデータを追加する
          $scope.todos.push(obj);
        } else {
          skippedItem++;
        }
      }

      $scope.importErrorIcon  = "infoCircle";
      if (skippedItem > 0) {
        $scope.importErrorColor = "yellow";
        $scope.importErrorMsg   = "インポートされなかったデータが" + skippedItem + "件あります。";
      } else {
        $scope.importErrorColor = "green";
        $scope.importErrorMsg   = "データをインポートしました。";

        $scope.jsonData = "";
      }
    }




    /* 「ToDoの追加」ダイアログ用 */
    $scope.open = function() {
      is_newTask = true;
      $scope.openType = "追加";
      $scope.dlgIcon = "new";

      $scope.task = "";
      $scope.prio = 1;
      $scope.memo = "";

      $scope.dlgToDo = "visible";
    };

    $scope.edit = function(task) {
      editingIndex  = lookupIndex(task.id);

      is_newTask = false;
      $scope.openType = "編集";
      $scope.dlgIcon = "editBox";

      $scope.task = $scope.todos[editingIndex].task;
      $scope.prio = $scope.todos[editingIndex].prio;
      $scope.memo = $scope.todos[editingIndex].memo;

      $scope.dlgToDo = "visible";
    };

    $scope.close = function() {
      if (is_newTask == true) {
        var new_todo = {
          id   : id++,
          task : $scope.task,
          prio : $scope.prio,
          memo : $scope.memo,
          done : false,
          showToDo : true,
        };
        $scope.todos.unshift(new_todo);
      } else {
        $scope.todos[editingIndex].task = $scope.task;
        $scope.todos[editingIndex].prio = $scope.prio;
        $scope.todos[editingIndex].memo = $scope.memo;
      }
      syncLocalStorage();

      $scope.dismissDialog();
    };

    $scope.getTaskDecoration = function(task) {
      var index = lookupIndex(task.id);
      if (index != undefined && $scope.todos[index].done == true) {
        return "text-decoration: line-through;";
      } else {
        return "";
      }
    };

    $scope.dismissDialog = function() { $scope.dlgToDo = "hidden";  };

    /* 「設定」ダイアログ用 */
    $scope.showConfig = function() {
      $scope.showDoneTask_bkup = $scope.showDoneTask;
      $scope.dlgConfig = "visible";
    };

    $scope.closeConfig = function() {
      if ($scope.showDoneTask == true) {
        for (i in $scope.todos) {
          $scope.todos[i].showToDo = true;
        }
      } else {
        for (i in $scope.todos) {
          $scope.todos[i].showToDo = !Boolean($scope.todos[i].done);
        }
      }
      $scope.dlgConfig = "hidden";
    };

    $scope.dismissConfig = function() {
      $scope.showDoneTask = $scope.showDoneTask_bkup;
      $scope.dlgConfig = "hidden";
    };

    /* 「About...」ダイアログ用 */
    $scope.showAbout  = function() { $scope.dlgAbout = "visible"; };
    $scope.closeAbout = function() { $scope.dlgAbout = "hidden";  };

    $scope.clearLocalStorageData = function() {
      $scope.showConfirm(
        "ToDoデータのクリア",
        "local storageに保存されているToDoデータをクリアしますか？",
        function() { clearLocalStorageData2(); }
      );
    };

    clearLocalStorageData2 = function() {
      localStorage.removeItem(MY_APP_KEY);
      $scope.disableLocalStorage = true;
      $scope.storage_clear_info = "local storageへの保存を有効化する場合は、Webページを再読み込みしてください。";
    }

    /* サンプルToDoデータを削除する */
    $scope.removeAllSampleData = function() {

      for (var i = $scope.todos.length - 1; i >=0; i--) {
        if ($scope.todos[i].sample == true) {
          $scope.todos.splice(i, 1);
        }
      }
      $scope.showRemoveAllSample = true;

      syncLocalStorage();
    };

    $scope.showMemo = function(memo) {
      if (memo == "") {
        return "(メモなし)"
      } else {
        return memo;
      }
    }

    /*
     * 「Confirm」ダイアログ用
     */
    $scope.showConfirm = function(title, msg, ok_func, no_func) {
      $scope.confirmTitle = title;
      $scope.confirmMsg   = msg;

      $scope.confirmTrueFunc  = ok_func;
      $scope.confirmFalseFunc = no_func;

      $scope.showFlgConfirmDlg = "visible";

      return false;
    }

    $scope.closeConfirmYes = function() {
      $scope.showFlgConfirmDlg = "hidden";
      if ($scope.confirmTrueFunc != undefined) {
        $scope.confirmTrueFunc();
      }
      return true;
    }

    $scope.closeConfirmNo = function() {
      $scope.showFlgConfirmDlg = "hidden";
      if ($scope.confirmFalseFunc != undefined) {
        $scope.confirmFalseFunc();
      }
      return true;
    }

    /*
     * $scope.todos[]にはAngularJSが追加した要素が含まれており、そのまま保存・ロード
     * すると、ページ再読み込みの際に"Error: ngRepeat:dupes Duplicate Key in Repeater"が
     * 発生する。そのため、localStorageには必要な要素のみでObjectを作り直して保存する。
     */
    syncLocalStorage = function() {
      if ($scope.disableLocalStorage == true) {
        return;
      }

      var save_obj = [];
      for (var i in $scope.todos) {
        var obj = {
          id   : $scope.todos[i].id,
          task : $scope.todos[i].task,
          prio : $scope.todos[i].prio,
          memo : $scope.todos[i].memo,
          done : $scope.todos[i].done,
          sample : $scope.todos[i].sample,
        };
        save_obj.push(obj);
      }
      localStorage.setItem(MY_APP_KEY, JSON.stringify(save_obj)); 
    };

    $scope.closeExport();
    $scope.closeImport();
    $scope.closeConfirmNo();
  }
]);

