/* officeui_sample.js */

var app = angular.module('OfficeUI_SampleApp', []);

app.controller('OfficeUI_SampleAppCtrl', ['$scope',
  function($scope) {

    $scope.showAbout = function() {
      $scope.isAboutShow = false;
    }

    $scope.closeAbout = function() {
      $scope.isAboutShow = true;
    }

    $scope.closeAbout();
  }
]);

app.controller('LabelSampleCtrl', ['$scope',
  function($scope) {
    $scope.msFontColors = [
      "alert", "black", "blue", "blueDark", "blueLight", "blueMid",
      "error", "green", "greenDark", "greenLight", "info", "magenta", "magentaDark",
      "magentaLight", "neutralDark", "neutralLight", "neutralLighter", "neutralLighterAlt",
      "neutralPrimary", "neutralSecondary", "neutralSecondaryAlt", "neutralTertiary",
      "neutralTertiaryAlt", "officeAccent1", "officeAccent10", "officeAccent2", "officeAccent3",
      "officeAccent4", "officeAccent5", "officeAccent6", "officeAccent7", "officeAccent8", "officeAccent9",
      "orange", "orangeLight", "purple", "purpleDark", "purpleLight", "red", "redDark", "success",
      "teal", "tealDark", "tealLight", "themeDark", "themeDarkAlt", "themeDarker", "themeLight",
      "themeLighter", "themeLighterAlt", "themePrimary", "themeSecondary", "themeTertiary",
      "white", "yellow", "yellowLight",
    ];

    $scope.showHtmlCode = function(color) {
      $scope.currentColor = color;
      var html =
        '<label class="ms-Label ms-fontColor-' + color + '">' + "\n" +
        '  かしこまっ！' + "\n" +
        '</label>';
      $scope.html = html;
    }

    $scope.currentColor = "magentaLight";
    $scope.radioColor = $scope.currentColor;
  }
]);

app.controller('FontsizeSampleCtrl', ['$scope',
  function($scope) {
    $scope.sizes = [
      "ms-fontSize-su",    "ms-fontSize-xxl", "ms-fontSize-xl",    "ms-fontSize-l",
      "ms-fontSize-mPlus", "ms-fontSize-m",   "ms-fontSize-sPlus", "ms-fontSize-s",
      "ms-fontSize-xs",    "ms-fontSize-mi",
    ];

    $scope.showHtmlCode = function(size) {
      if (size == undefined) {
        $scope.currentSize = $scope.currentSize;
      } else {
        $scope.currentSize = size;
      }
      var html =
        '<label class="ms-Label ' + size + '">' + "\n" +
        '  ' + $scope.labelString + "\n" +
        '</label>';
      $scope.html = html;
    }

    $scope.labelString = "かしこまっ！";

    $scope.currentSize = $scope.sizes[0];
    $scope.radioSize = $scope.currentSize;

    $scope.showHtmlCode($scope.radioSize);
  }
]);

app.controller('ButtonSampleCtrl', ['$scope',
  function($scope) {
    $scope.buttonTypes = [
      "Button", "Button--primary", "Button--hero", "Button--compound", "Button--command",
    ];
    $scope.showHtmlCode = function(button) {
      var html =
        '<button class="ms-Button ms-' + button + '">' + "\n" +
        '  <span class="ms-Button-label">ms-' + button + '</span>' + "\n" +
        '</button>';
      $scope.html = html;
    }
  }
]);

app.controller('ToggleSampleCtrl', ['$scope',
  function($scope) {
    $scope.toggleLabel = "トグルボタン";
    $scope.toggleOnLabel  = "ON";
    $scope.toggleOffLabel = "OFF";

    $scope.toggle = false;
    $scope.toggleTextLeft = false;

    $scope.checkTextLeft = function() {
      if ($scope.toggleTextLeft == true) {
        $scope.styleTextToggle = " ms-Toggle--textLeft";
      } else {
        $scope.styleTextToggle = "";
      }
      $scope.showHtmlCode();
    }

    $scope.showHtmlCode = function() {
      var html =
        '<span class="ms-Toggle' + $scope.styleTextToggle + '">' + "\n" +
        '  <span class="ms-Toggle-description">' + $scope.toggleLabel + '</span>' + "\n" +
        '  <input id="toggle1" type="checkbox" class="ms-Toggle-input">' + "\n" +
        '  <label for="toggle1" class="ms-Toggle-field">' + "\n" +
        '    <span class="ms-Label ms-Label--off">' + $scope.toggleOffLabel + '</span>' + "\n" +
        '    <span class="ms-Label ms-Label--on">' + $scope.toggleOnLabel + '</span>' + "\n" +
        '  </label>' + "\n" +
        '</span>';
      $scope.html = html;
    }

    $scope.checkTextLeft();
    //$scope.showHtmlCode();
  }
]);

app.controller('CheckSampleCtrl', ['$scope',
  function($scope) {
    $scope.checkBoxes = [
      { name : "らぁら",   icon : "icon_raara.jpg" },
      { name : "みれぃ",   icon : "icon_mirei.jpg" },
      { name : "そふぃ",   icon : "icon_sofy.jpg"  },
      { name : "シオン",   icon : "icon_sion.jpg"  },
      { name : "ドロシー", icon : "icon_doro.jpg"  }, 
      { name : "レオナ",   icon : "icon_reo.jpg"   }
    ];

    $scope.checkBoxID = "check1";
    $scope.checkBoxLabel = "チェックボックスのサンプル";

    $scope.check1 = false;

    $scope.showHtmlCode = function() {
      var html =
        '<div class="ms-ChoiceField">' + "\n" +
        '  <input id="' + $scope.checkBoxID + '" class="ms-ChoiceField-input" type="checkbox">' + "\n" +
        '  <label for="' + $scope.checkBoxID + '" class="ms-ChoiceField-field">' + "\n" +
        '    <span class="ms-Label">' + $scope.checkBoxLabel + '</span>' + "\n" +
        '  </label>' + "\n" +
        '</div>';
      $scope.html = html;
    }
    $scope.showHtmlCode();
  }
]);

app.controller('RadioSampleCtrl', ['$scope',
  function($scope) {
    var id = 0;

    var labelItems = [
      "らぁら", "みれぃ", "そふぃ", "シオン", "ドロシー", "レオナ", "ファルル",
      "あろま", "みかん", "コスモ", "ふわり", "栄子", "なぎさ", "のどか", "ななみ",
      "栃乙女 愛(とちおとめ ラブ)", "はなな", "蘭たん", "香川 いろは",
    ];

    getLabel = function() {
      var len = labelItems.length - 1;
      return labelItems[parseInt(Math.random() * len)];
    }

    $scope.radioBoxes = [
      { shadow_id : id, id : 'radio' + id, label : "らぁら", value : id++ },
      { shadow_id : id, id : 'radio' + id, label : "みれぃ", value : id++ },
      { shadow_id : id, id : 'radio' + id, label : "そふぃ", value : id++ },
      { shadow_id : id, id : 'radio' + id, label : getLabel() + id, value : id++ }
    ];
    $scope.radio = $scope.radioBoxes[1].value;
    $scope.radioName = $scope.radio;
    $scope.radioGroupName = "radioGroup1";

    $scope.addItem = function() {
      var obj = { shadow_id : id, id : 'radio' + id, label : getLabel() + id, value : id++ };
      $scope.radioBoxes.push(obj);
      $scope.showHtmlCode();
    };

    $scope.removeItem = function(radio) {
      if ($scope.radioBoxes.length == 1) {
        return;
      }
      for (var i in $scope.radioBoxes) {
        if ($scope.radioBoxes[i].shadow_id == radio.shadow_id) {
          $scope.radioBoxes.splice(i, 1);
        }
      }
      $scope.showHtmlCode();
    }

    $scope.setRadioValue = function(radio) {
      // 動的にラジオボタンを生成する場合は、ng-clickで
      // イベント取得・値設定しないとダメっぽい
      $scope.radioName = radio.value;
    }

    $scope.showHtmlCode = function() {
      var html = '<div class="ms-ChoiceFieldGroup">' + "\n";
      for (var i in $scope.radioBoxes) {
        html +=
          '  <div class="ms-ChoiceField">' + "\n" +
          '    <input id="' + $scope.radioBoxes[i].id + '" class="ms-ChoiceField-input" type="radio" value="' + $scope.radioBoxes[i].value + '" name="' + $scope.radioGroupName + '">' + "\n" +
          '    <label for="' + $scope.radioBoxes[i].id + '" class="ms-ChoiceField-field">' + "\n" +
          '      <span class="ms-Label">' + $scope.radioBoxes[i].label + '</span>' + "\n" +
          '    </label>' + "\n" +
          '  </div>' + "\n";
      }
      html += '</div>';

      $scope.html = html;
    }
    $scope.showHtmlCode();
  }
]);

app.controller('TextFieldSampleCtrl', ['$scope',
  function($scope) {

    $scope.FieldStyle = "field";
    $scope.coexistLabelAndPlaceHolder = true;
    $scope.labelAddRequired = false;

    $scope.textLabel = "ラベル";
    $scope.textDescription= "テキストフィールド入力内容の説明";
    $scope.textPlaceHolder = "ここにテキストを入力";

    /* TextFieldのサンプルは直接DOMを操作する方法で生成する */
    $scope.showHtmlCode = function() {
      var addRequired = ($scope.labelAddRequired == true) ? 'is-required' : '';
      var html = '';
      if ($scope.FieldStyle == "field") {
        if ($scope.coexistLabelAndPlaceHolder == false) {
          html +=
            '<div class="ms-TextField ms-TextField--placeholder">' + "\n" +
            '  <label class="ms-Label ' + addRequired + '">' + $scope.textLabel + '</label>' + "\n" +
            '  <input class="ms-TextField-field">' + "\n";
        } else {
          html +=
            '<div class="ms-TextField">' + "\n" +
            '  <label class="ms-Label ' + addRequired + '">' + $scope.textLabel + '</label>' + "\n" +
            '  <input class="ms-TextField-field" placeholder="' + $scope.textPlaceHolder + '">' + "\n";
        }
        if ($scope.textDescription != "") {
          html += ' <span class="ms-TextField-description">' + $scope.textDescription + '</span>' + "\n";
        }
      } else if ($scope.FieldStyle == "underlined") {
        html +=
          '<div class="ms-TextField ms-TextField--underlined">' + "\n" +
          '  <label class="ms-Label ' + addRequired + '">' + $scope.textLabel + '</label>' + "\n" +
          '  <input class="ms-TextField-field"';
          if ($scope.textPlaceHolder != "") {
            html +=
              ' placeholder="' + $scope.textPlaceHolder + '">' + "\n";
          } else {
            html += '>';
          }
      } else if ($scope.FieldStyle == "multiline") {
        html +=
          '<div class="ms-TextField ms-TextField--multiline">' + "\n" +
          '  <label class="ms-Label ' + addRequired + '">' + $scope.textLabel + '</label>' + "\n" +
          '  <textarea class="ms-TextField-field"';
          if ($scope.textPlaceHolder != "") {
            html +=
              ' placeholder="' + $scope.textPlaceHolder + '"></textarea>' + "\n" +
              '  <span class="ms-TextField-description">' + $scope.textDescription + '</span>' + "\n";
          } else {
            html +=
              '></textarea>' + "\n";
              '  <span class="ms-TextField-description">' + $scope.textDescription + '</span>' + "\n";
          }
      } else {
        return;
      }
      html += '</div>';

      document.getElementById("myTextField").innerHTML = html;
      $scope.html = html;
    }

    $scope.showHtmlCode();

  }
]);

app.controller('DropdownCtrl', ['$scope',
  function($scope) {
    var id = 0;

    var listItems = [
      "ハイビスカスサマーコーデ", "真っ赤なハイビスカスサマーコーデ",
      "わたあめふわふわコーデ", "ポップコーンコーデ", "チョコホイップまぜまぜコーデ"
    ];
    var listIcons = [
      "star", "starEmpty", "ribbon", "lightning", "heart", "heartEmpty", "soccer",
      "balloon", "cat", "music", "coffee", "dogAlt", "plane", "lightBulb"
    ];

    getLabel = function() {
      var len = listItems.length - 1;
      return listItems[parseInt(Math.random() * len)];
    }

    getIcon = function() {
      var len = listIcons.length - 1;
      return listIcons[parseInt(Math.random() * len)];
    }

    $scope.lists = [
      { id: id, value: id++, icon: getIcon(), label: "ハイビスカスサマーコーデ" },
      { id: id ,value: id++, icon: getIcon(), label: "真っ赤なハイビスカスサマーコーデ" },
      { id: id, value: id++, icon: getIcon(), label: "わたあめふわふわコーデ" },
      { id: id, value: id++, icon: getIcon(), label: "ポップコーンコーデ" },
      { id: id, value: id++, icon: getIcon(), label: "チョコホイップまぜまぜコーデ" },
    ];
    $scope.codeValue = $scope.lists[2];

    updateOptionItem = function() {
      var obj = document.getElementById("__my_select");
      var html =
        '<span class="ms-Dropdown-title">' +
        //'<i class="ms-Icon ms-Icon--' + $scope.lists[0].icon + '"></i>&nbsp;' +
        $scope.lists[0].label +
        '</span>' +
        '<ui class="ms-Dropdown-items">';
      for (i in $scope.lists) {
        html +=
          '<li class="ms-Dropdown-item">' +
          //'<i class="ms-Icon ms-Icon--' + $scope.lists[i].icon + '"></i>' +
          $scope.lists[i].label +
          '</li>';
      }
      html += '</ui>';

      obj.innerHTML = html;
    }

    $scope.addItem = function() {
      var obj = { id : id, icon: getIcon(), label: getLabel() + id, value: id++ };

      $scope.lists.push(obj);
      updateOptionItem();

      $scope.showHtmlCode();
    }

    $scope.removeItem = function(list) {
      if ($scope.lists.length == 1) {
        return;
      }
      for (var i in $scope.lists) {
        if ($scope.lists[i].id == list.id) {
          $scope.lists.splice(i, 1);
        }
      }
      updateOptionItem();

      $scope.showHtmlCode();
    }

    $scope.showHtmlCode = function() {
      $scope.html = "";
    }

    $scope.showHtmlCode();
  }
]);

app.controller('TableSampleCtrl', ['$scope',
  function($scope) {
    $scope.showHtmlCode = function() {
      $scope.html = "";
    }
    $scope.showHtmlCode();
  }
]);

