/* fabric_sample01.js */

FabricSample = (function () {

  var count = 0;

  function init() {
    countupKashikoma();
    updateCheckbox();
    updateRadioButton();
  };

  function countupKashikoma() {
    document.getElementById("lbl_button").innerHTML = "ボタン  (" + (count++) + " かしこまっ！)";
  };

  function cyalumeChange() {
    var msg = document.getElementById("lbl_toggle");
    var chk = document.getElementById("cyalume-change").checked;

    if (chk == true) {
      msg.innerHTML = "トグルボタン  (サイリウムチェンジ)";
    } else {
      msg.innerHTML = "トグルボタン";
    }
  };

  function updateCheckbox() {
    var chk_box_ids = [ "happy_pa_lucky", "dream_parade" ];
    var html = "選択した曲： ";

    for (var i in chk_box_ids) {
      var elm = document.getElementById(chk_box_ids[i]);
      var chk = elm.checked;
      if (chk == true) {
        html += document.getElementById("lbl_" + chk_box_ids[i]).innerHTML + ", ";
      }
    }
    document.getElementById("lbl_checkbox").innerHTML = html;
  }

  function updateRadioButton() {
    var solami_smile = "";
    var dressing_pafe = "";

    var elm = document.getElementsByName("solami_smile");
    for (var i in elm) {
      if (elm[i].checked == true) {
        solami_smile = elm[i].value;
        break;
      }
    }

    var elm = document.getElementsByName("dressing_pafe");
    for (var i in elm) {
      if (elm[i].checked == true) {
        dressing_pafe = elm[i].value;
        break;
      }
    }

    document.getElementById("lbl_radio").innerHTML =
      "ラジオボタン(ChoiceField)  " + solami_smile + " & " + dressing_pafe;
  }

  return {
    "init" : init,
    "countupKashikoma"  : countupKashikoma,
    "cyalumeChange"     : cyalumeChange,
    "updateCheckbox"    : updateCheckbox,
    "updateRadioButton" : updateRadioButton,
  };
}());

