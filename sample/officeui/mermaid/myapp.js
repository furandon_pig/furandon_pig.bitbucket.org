/* myapp.js */

var app = angular.module('MyApp', []);
app.controller('MyAppCtrl', ['$scope',
  function($scope) {

    /* コントローラの初期化 */
    var MY_APP_KEY        = "OfficeUI_MermaidSample";
    var MY_APP_KEY_CONFIG = "OfficeUI_MermaidSampleConfig";

    var errorCount = 0;
    var lastSuccessTime = 0;

    /* mermaidライブラリの初期化 */
    initMermaid = function() {
      var mermaid_config = {
        startOnLoad:false
      };
      mermaidAPI.initialize(mermaid_config);
    }

    /* graphのレンダリング */
    // レンダリング完了時のコールバック関数
    successRendering = function(html) {
      document.getElementById('graph').innerHTML = html;

      var a = document.getElementById('graph');
      if (window.navigator.userAgent.toLowerCase().indexOf('safari') != -1) {
        a.children.item().setAttribute('width',  a.getAttribute('viewbox').split(' ')[2]|0);
        a.children.item().setAttribute('height', a.getAttribute('viewbox').split(' ')[3]|0);
      } else {
        a.children.graph.setAttribute('width',  a.getAttribute('viewbox').split(' ')[2]|0);
        a.children.graph.setAttribute('height', a.getAttribute('viewbox').split(' ')[3]|0);
      }

      $scope.resultSVG =
        '<?xml version="1.0" encoding="UTF-8" standalone="no"?>' + "\n" +
        '<!-- Created with mermain.js sample -->' + "\n" +
        '<!-- http://http://furandon_pig.bitbucket.org/sample/officeui/mermaid/ -->' + "\n" +
        '<svg' + "\n" +
        '   xmlns:dc="http://purl.org/dc/elements/1.1/"' + "\n" +
        '   xmlns:cc="http://creativecommons.org/ns#"' + "\n" +
        '   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"' + "\n" +
        '   xmlns:svg="http://www.w3.org/2000/svg"' + "\n" +
        '   xmlns="http://www.w3.org/2000/svg"' + "\n" +
        '   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"' + "\n" +
        '   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"' + "\n" +
        '   width="210mm"' + "\n" +
        '   height="297mm"' + "\n" +
        '   viewBox="0 0 744.09448819 1052.3622047"' + "\n" +
        '   id="svg3358"' + "\n" +
        '   version="1.1"' + "\n" +
        '   inkscape:version="0.91 r13725"' + "\n" +
        '   sodipodi:docname="sample.svg">' + "\n" +
        convertInkscapeFormat(html);
    }

    convForignObject = function(args) {
      var x = (args[1]|0) / 10;
      var y = (args[2]|0) / 2 + 6;
      return '<text width="' + args[1] + '" height="' + args[2] + '" x="' + x + '" y="' + y + '">';
    }

    convTspan = function(args) {
      return '<tspan>' + args[1] + '</tspan>';
    }

    // 生成されたSVGをInkscapeで利用できる形に変換
    convertInkscapeFormat = function(svg) {
      var tags = []; 
      var split_each_tag = new RegExp('><', 'g');
      var tags = svg.replace(split_each_tag, '>__SPLITER__<').split('__SPLITER__');

      // 置換用リスト
      var elms = [ 
        {
          regexp: new RegExp('<foreignObject.*?width="([0-9]*)".*height="([0-9]*)".*>'),
          conv: convForignObject
        }, {
          regexp: new RegExp('</foreignObject>'),
          conv: function() { return '</text>'; }
        }, {
          regexp: new RegExp('<div style="display: inline-block;.*>(.*)</div>'),
          conv: convTspan
        }, {
          regexp: new RegExp('<svg .*?>'),
          conv: function() { return "" }
        },
      ];

      var result = [];
      for (var i in tags) {
        var is_match = false;
        for (var j in elms) {
          if (tags[i].match(elms[j].regexp) != null) {
            result.push(tags[i].replace(elms[j].regexp, function(){ return elms[j].conv(arguments); }));
            is_match = true;
            break;
          }
        }
        if (is_match == false) {
            result.push(tags[i]);
        }
      }

      return result.join("\n");
    }

    // 許容するエラーの上限を超過していたらレンダリングしない
    judgeSkipRendering = function() {
      var interval = Date.now() - lastSuccessTime;
      if (interval < 1000 && errorCount > 100) {
        return false;
      } else {
        return true;
      }
    }

    // テキスト入力時に呼ばれる関数
    $scope.render = function() {
      if (judgeSkipRendering() == false) {
        return;
      }

      if (CodeInputHelper.getLastCheckResult() == false) {
        alertErr('不正な文字入力。 ' + CodeInputHelper.getLastInvalidString());
        return;
      }

      // HTMLを含むラベルをエスケープする
      var esc_langle = new RegExp('<', 'g');
      var escaped_code = $scope.code.replace(esc_langle, '&lt;');

      // 属性を持たないHTMLタグに限定してエスケープを戻す
      var allow_tags = [
        { regexp: new RegExp('&lt;br>',  'ig'), newstr: '<br>' },
        /*
        { regexp: new RegExp('&lt;b>',   'ig'), newstr: '<b>'  },
        { regexp: new RegExp('&lt;/b>',  'ig'), newstr: '</b>' },
        { regexp: new RegExp('&lt;u>',   'ig'), newstr: '<u>'  },
        { regexp: new RegExp('&lt;/u>',  'ig'), newstr: '</u>' },
        { regexp: new RegExp('&lt;i>',   'ig'), newstr: '<i>'  },
        { regexp: new RegExp('&lt;/i>',  'ig'), newstr: '</i>' },
        { regexp: new RegExp('&lt;s>',   'ig'), newstr: '<s>'  },
        { regexp: new RegExp('&lt;/s>',  'ig'), newstr: '</s>' },
        */
      ];
      for (i in allow_tags) {
        escaped_code = escaped_code.replace(allow_tags[i].regexp, allow_tags[i].newstr);
      }

      var old_graph = document.getElementById('graph').innerHTML;
      try {
        document.getElementById('graph').innerHTML = '';
        mermaidAPI.render('graph', escaped_code, successRendering);
      } catch(e) {
        document.getElementById('graph').innerHTML = old_graph;
        errorCount++;
        alertErr("レンダリングエラー");

        // エラー時にはDOM要素が残ったままになるので削除する
        document.getElementById("dgraph").remove();

        return;
      }
      errorCount = 0;
      lastSuccessTime = Date.now();
      alertInfo("グラフをレンダリングしました。");
    }

    /*
     * アラート表示用
     */
    alertInfo = function(msg) {
      $scope.alertColor = "green";
      $scope.alertIcon  = "infoCircle";
      $scope.alertMsg   = msg;
    }

    alertWarn = function(msg) {
      $scope.alertColor = "yellow";
      $scope.alertIcon  = "alert";
      $scope.alertMsg   = msg;
    }

    alertErr = function(msg) {
      $scope.alertColor = "red";
      $scope.alertIcon  = "alert";
      $scope.alertMsg   = msg;
    }

    var defaultMsg = "";
    var appMsg = defaultMsg;
    alertInfo(appMsg);

    /* local storageからデータを読み込む */
    loadData = function() {
      var json_str = localStorage.getItem(MY_APP_KEY);
      if (json_str != null) {
        try {
          var items= JSON.parse(json_str);
          if (items.length == 0) {
            $scope.code = generateSampleData();
          } else {
            var esc_newline = new RegExp('__BR__', 'g');
            $scope.code = items[0].code.replace(esc_newline, "\n");
          }
        } catch(e) {
          console.log('-=> JSON parse error!');
          $scope.code = generateSampleData();
        }
      } else {
        $scope.code = generateSampleData();
      }
    }

    generateSampleData = function() {
      var code =
        "graph LR\n" +
        "\n" +
        '  386bsd("386BSD")' + "\n" +
        '  fbsd1("FreeBSD 1.0")' + "\n" +
        '  nbsd08("NetBSD 0.8")' + "\n" +
        '  nbsd11("NetBSD 1.1")' + "\n" +
        '  obsd20("OpenBSD 2.0")' + "\n" +
        "\n" +
        '  fbsd("FreeBSD")' + "\n" +
        '  nbsd("NetBSD")' + "\n" +
        '  obsd("OpenBSD")' + "\n" +
        "\n" +
        '386bsd-.->fbsd1' + "\n" +
        '386bsd-.->nbsd08' + "\n" +
        'nbsd08-->nbsd11' + "\n" +
        'nbsd11-.->obsd20' + "\n" +
        "\n" +
        'fbsd1-->fbsd' + "\n" +
        'nbsd11-->nbsd' + "\n" +
        'obsd20-->obsd';
      return code;
    }

    $scope.save = function() {
      try {
        saveData();
        alertInfo("データを保存しました。");
      } catch(e) {
        var msg =
          "データ保存時にエラーが発生しました。\n" +
          'local storageのデータkeyは"' + MY_APP_KEY + '"です。';
        alertErr(msg);
      }
    }

    /*
     * ダイアログボックス用のテキストを初期化
     */
    $scope.disableLocalStorage = false;
    $scope.storageClearInfo = "保存してあるデータを全て削除します。";

    $scope.clearLocalStorageData = function() {
      $scope.showConfirm(
          "データのクリア",
          "保存してあるデータをクリアしますか？",
          function() { clearLocalStorageData2() }
      );
    }

    clearLocalStorageData2 = function() {
      localStorage.removeItem(MY_APP_KEY);
      localStorage.removeItem(MY_APP_KEY_CONFIG);

      $scope.disableLocalStorage = true;
      //$scope.storageClearInfo = "データの保存を有効化する場合は、Webページを再読み込みしてください。";
      $scope.storageClearInfo = "このアプリが保存していたlocal storageのデータを削除しました。";
    }

    getJSONString = function() {

      /*
       * 後の拡張用にObjectの配列の形で保存する。
       */
      var objs = [
        { code: $scope.code },
      ];

      /*
       * 人間が視認しやすい形のJSONテキストを生成する
       *   - keyとvalueの間のコロン「:」の前後には空白を入れない
       *   - Objectの最後のvalueの末尾にはカンマ「,」を入れない
       *   - 数値、論理値(Boolean)はクォート「"」で挟まない
       */
      var esc_quote = new RegExp('"', 'g');
      var esc_newline = new RegExp("\n", 'g');

      var is_first_bracket = true;
      var json_str = "[\n";
      for (var i in objs) {
        var tmp = '';
        if (is_first_bracket == true) {
          tmp = '  {' + "\n";
          is_first_bracket = false;
        }

        tmp += '    "code":' + '"' +
          objs[i].code.replace(esc_quote, '\\"').replace(esc_newline, '__BR__') + '"' + "\n";

        if (i == (objs.length - 1)) {
          tmp += '  }';
        } else {
          tmp += '  }, {';
        }

        json_str = json_str + tmp + "\n";
      }
      json_str += ']';

      return json_str;
    }

    saveData = function() {
      localStorage.setItem(MY_APP_KEY, getJSONString());
    }

    /*
     * ダウンロード機能
     */
    $scope.download = function() {
      var blob = new Blob([$scope.resultSVG]);
      var url = window.URL || window.webkitURL;
      var blobURL = url.createObjectURL(blob);

      var a = document.createElement('a');
      a.download = "mermaid.svg";
      a.href = blobURL;
      a.click();  
    }

    /*
     * 「ノード追加」ダイアログ用
     */
    $scope.openNodeDialog = function() {
      $scope.showFlgNodeDlg = "visible";
    }

    $scope.closeNodeDialog = function() {
      $scope.showFlgNodeDlg = "hidden";
    }

    $scope.dismissNodeDialog = function() {
      $scope.showFlgNodeDlg = "hidden";
    }

    /*
     * 「設定」ダイアログ用
     */
    $scope.closeConfig = function() {
      if ($scope.disableLocalStorage == false) {
        var obj = {
          cfgSearchFs   : $scope.cfgSearchFs,
          cfgSearchNs   : $scope.cfgSearchNs,
          cfgSearchMemo : $scope.cfgSearchMemo,
          cfgHideSample : $scope.cfgHideSample,
        };
        localStorage.setItem(MY_APP_KEY_CONFIG, JSON.stringify(obj));
      }

      $scope.showFlgConfigDlg = "hidden";
    };

    $scope.showConfig  = function() {
      $scope.showFlgConfigDlg = "visible";
    }

    $scope.dismissConfig = function() {
      $scope.showFlgConfigDlg = "hidden";
    }

    /*
     * 「About...」ダイアログ用
     */
    $scope.showAbout  = function() { $scope.showFlgAboutDlg = "visible"; }
    $scope.closeAbout = function() { $scope.showFlgAboutDlg = "hidden";  }

    /*
     * 「Confirm」ダイアログ用
     */
    $scope.showConfirm = function(title, msg, ok_func, no_func) {
      $scope.confirmTitle = title;
      $scope.confirmMsg   = msg;

      $scope.confirmTrueFunc  = ok_func;
      $scope.confirmFalseFunc = no_func;

      $scope.showFlgConfirmDlg = "visible";

      return false;
    }

    $scope.closeConfirmYes = function() {
      $scope.showFlgConfirmDlg = "hidden";
      if ($scope.confirmTrueFunc != undefined) {
        $scope.confirmTrueFunc();
      }
      return true;
    }

    $scope.closeConfirmNo = function() {
      $scope.showFlgConfirmDlg = "hidden";
      if ($scope.confirmFalseFunc != undefined) {
        $scope.confirmFalseFunc();
      }
      return true;
    }

    $scope.showHelp = function() {
      $scope.showHelpArea = "block";
      //$scope.helpIcon = "lifesaverLock";
      $scope.helpIcon = "hide";
    }

    $scope.hideHelp = function() {
      $scope.showHelpArea = "none";
      $scope.helpIcon = "lifesaver";
    }

    $scope.toggleHelp = function() {
      if ($scope.showHelpArea == "none") {
        $scope.showHelp();
      } else {
        $scope.hideHelp();
      }
    }

    /*
     * ページ読み込み時にダイアログを全て閉じる
     */
    $scope.dismissConfig();
    $scope.closeAbout();
    $scope.closeConfirmNo();
    $scope.hideHelp();

    loadData();

    $scope.render();

    // 「ダウンロード」機能が利用できるのはChoromeのみ
    if (window.navigator.userAgent.toLowerCase().indexOf('chrome') != -1) {
      $scope.hideDownload = false;
    } else {
      $scope.hideDownload = true;
      alertInfo("ブラウザがChromeの場合は、作成したグラフをSVG形式でダウンロードできます。");
    }
  }
]);

