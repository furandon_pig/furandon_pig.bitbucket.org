..

NetBSDの設定
============

マシンの起動とログイン
----------------------

UNIX系OS、Mac、Windows用にそれぞれの環境用で仮想マシン起動用のスクリプトを用意しています。以下に起動スクリプトの実行例を示します。

仮想マシンのイメージファイルは以下のURLからダウンロードしてください。

 * `NetBSD仮想マシンイメージファイル <http://192.168.0.100:8080/vm.tar.gz>`_

UNIX系OSの場合
::::::::::::::

::

 $ ./start/unix/nbsd601.sh

Macの場合
:::::::::

 $ ./start/osx/nbsd601.sh

Windowsの場合
:::::::::::::

 $ ./start/windows/nbsd601.bat

起動後の設定
------------

起動後の仮想マシンは、インストール直後の状態となっています。そこに各人でユーザ作成・パッケージ導入の手順を実施する形になります。

rootユーザでのログイン
----------------------

仮想マシン起動後、rootユーザでログインします。パスワードは「yry2」です。

これは勉強会用に安易なパスワードにしていますが、実際は強い(バレにくい)パスワードを設定するよう注意してください。

DHCPによるIPアドレス取得
::::::::::::::::::::::::

qemuのVGAでは、キーマッピングが一部おかしいものがあり、"+"が入力できない等の問題があります。このため、qemuのVGAでの作業は最小限に止め、残りの手順はsshからログインして行うようにします。

まずは以下のコマンドでIPアドレスを取得します。BSD系のOSでは、eth0等といった名前ではなく、re0,ep0といったネットワークドライバ名に由来するインタフェース名になっています。

::

 # ifconfig -a
 # dhclient re0
 # ifconfig re0

最低限必要なパッケージのインストール
------------------------------------

環境変数PKG_PATHの設定
::::::::::::::::::::::

まずは最低限必要なパッケージをインストールします。パッケージはインターネットからインストール可能であり、環境変数PKG_PATHにパッケージのURLを設定します。

rootでログインの後、/root/.profileを編集します。(この手順のみviを使用します)

::

 # vi /root/.profile

以下の行のコメント(行頭の「#」)を外します。'#'にカーソルを合わせ、'x'キーを押すと一文字削除になります。

::

 #export PKG_PATH=ftp://ftp.NetBSD.org/pub/pkgsrc/packages/NetBSD/$(uname -m)/6.0/All

その後、":w!"と入力し、ファイルを保存し、":q"でviを終了します。

パッケージのインストール
::::::::::::::::::::::::

一旦ログアウトし、再度rootユーザでログインします。環境変数PKG_PATHが設定されているかどうかを、以下のコマンドで確認します。

::

 # echo $PKG_PATH

パッケージの追加は、"pkg_add"コマンドで行います。"-v"オプションで詳細なメッセージ表示を有効にしています。

::

 # pkg_add -v nano
 # pkg_add -v bash
 # pkg_add -v zsh

グループの作成
--------------

グループとして、gorakubu(ごらく部)とseitokai(生徒会)を作ります。

::

 # groupadd -g 2000 gorakubu
 # groupadd -g 2001 seitokai
 # cat /etc/group

ユーザの作成
------------

ユーザを作成します。ここではユーザ「akari」を作成しています。

 # useradd -G wheel -g gorakubu -m -s /bin/sh akari

パスワードを設定します。ここではパスワードとして「yry2」を設定します。

 # passwd akari
 Changin password for akari.
 New Password:
 Retype New Password:
 Please enter a longer password.
 New Password:
 Retype New Password:
 #

sshサーバの起動
---------------

sshサーバを起動し、別のマシンからログインできるようにします。ファイル名やディレクトリ名はTABキーで補完できます。ssh鍵の生成には少し時間がかかります。

::

 # /etc/rc.d/sshd onestart


ここまででqemu VGA上での作業は完了です。あとはクライアント(ホストOS)からログインできます。

::

 $ ssh -p 2230 akari@localhost
 The authenticity of host '[localhost]:2230 ([127.0.0.1]:2230)' can't be established.
 RSA key fingerprint is c4:6a:ae:ea:cd:80:cd:8a:08:f7:0c:43:ae:46:86:64.
 Are you sure you want to continue connecting (yes/no)? yes
 Warning: Permanently added '[localhost]:2230' (RSA) to the list of known hosts.
 Password:
 NetBSD 6.0.1 (GENERIC)

  Welcome to NetBSD!

 $

sudoの設定
----------

::

 $ su -
 Password:
 Terminal type is xterm.
 # export EDITOR=nano
 # pkg_add -v sudo
 # visudo
 (F6キーを押し、「wheel」を検索する)

以下の行のコメント(行頭の「#」)を外す

::

 ## Uncomment to allow members of group wheel to execute any command
 %wheel ALL=(ALL) ALL

Ctrl-Xを押し、以下の質問に「Y」を入力する。

::

 Save modified buffer (ANSWERING "No" WILL DESTROY CHANGES) ?
 Y Yes
 N No            ^C Cancel

ファイル名はそのままでよいので、単にEnterキーを押す。

::

 File Name to Weite: /usr/pkg/etc/sudoers.tmp

'exit'コマンドを実行し、rootユーザから一般ユーザに戻る。

::

 # exit
 $ sudo id

 We trust you have received the usual lecture from the local System
 Administrator. It usually boils down to these three things:

     #1) Respect the privacy of others.
     #2) Think before you type.
     #3) With great power comes great responsibility.

 Password:
 uid=0(root) gid=0(wheel) groups=0(wheel),2(kmem),3(sys),4(tty),5(operator),20(staff),31(guest)
 $

ホスト名の設定
--------------

/etc/mynameに記載した内容がホスト名となります。末尾の改行を含めないよう注意してください。

::

 $ sudo nano /etc/myname
 vmnbsd601

起動時のサーバー設定
--------------------

/etc/rc.conf
::::::::::::

起動時に立ち上げるサーバ等の設定は、/etc/rc.confに記載します。

::

 $ sudo nano /etc/rc.conf

sshの設定
:::::::::

最後の行に以下を追記します。

::

 sshd=YES

DHCPクライアントの設定
::::::::::::::::::::::

先ほどの「sshd=YES」の後に以下を追記します。

::

 dhclient=YES
 dhclient_flags="re0"

その他
::::::

ゆるゆにでは使用しないデーモンをOFFにしておきます。

::

 postfix=NO
 powerd=NO

crontabの設定
-------------

::

 $ sudo /bin/sh
 # export EDITOR=nano
 # crontab -e
 # crontab -l

仮想マシンのシャットダウン
--------------------------

ここで一旦マシンをシャットダウンします。

::

 $ sudo shutdown -h now

ユーザ側での独自設定
--------------------

シェルの変更
::::::::::::

ユーザ追加時にシェルとして/bin/shを使用する設定にしてありますが、それでは不便という場合は、chshコマンドでシェルを変更します。pkg_addでbashをインストールしていますので、以下にshからbashにシェルを変更する手順を示します。

まず、whichコマンドでbashコマンドのパス(場所)を探した後、chshを起動します。
(NetBSDでは、パッケージのインストール先が/usr/pkg以下となっています)

::

 $ which bash
 /usr/pkg/bin/bash
 $ export EDITOR=nano
 $ chsh

シェルの指定が間違っていた場合は、以下のメッセージが出力されます。その場合は「y」を押して正しいシェルを設定し直します。

::

 chsh: /usr/pkg/bin/bash2: non-standard shell
 re-edit the password file? [y]:

設定が完了すれば、次回のログイン以降はbashが起動されるようになります。

Xを飛ばす
---------

ホストOS側でXサーバーが起動している場合、以下の手順でゲストOSのXクライアントを表示できます。

sshサーバの設定を行う必要があるため、sshd_configファイルを編集します。

::

 $ export EDITOR=nano
 $ sudo /etc/ssh/sshd_config

X11Forwardingという項目があるので、コメントアウトの後に"yes"を設定します。

::

 X11Forwarding yes

sshd_configファイルを保存の後、sshdを再起動します。

::

 $ sudo /etc/rc.d/sshd restart

クライアントからは"-X"オプションを付与してログインします。

::

 $ ssh -X -p 2230 akari@localhost

Xを飛ばす例として、leafpad(Windowsのメモ帳風のエディタ)の場合を示します。

::

 $ sudo -i pkg_add -v leafpad
 $ leafpad
