.. yry2_01 documentation master file, created by
   sphinx-quickstart on Sat Nov 24 03:54:14 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

#2 UNIXプログラミング入門
=========================

.. contents:: もくじ
   :depth: 3

第2回の内容
-----------

今回のゆるゆにR(ゆるいUNIX勉強会)では、UNIX開発環境にフォーカスを当ててみます。Eclipse等のIDEを使った開発ではなく、コマンドラインからGNU Binary Utilities(binutils)を利用したC言語によるプログラム開発を行ってみます。

C言語入門(かけ足で)
-------------------

hello,world.
^^^^^^^^^^^^

まずはhello,world.から。

.. highlight:: none

::

  /* 01_hello.c
   * gcc -Wall -Werror -g -o 01_hello 01_hello.c
   */

  #include <stdio.h>

  int main(int argc, char *argv[])
  {
          printf("hello,world.\n");
          return 0;
  }
    

以下の手順でコンパイルを行い、生成された実行ファイルを走らせてみます。

.. highlight:: none

::

  $ gcc -Wall -Werror -g -o 01_hello 01_hello.c
  $ ./01_hello
  hello,world.
  $
    

入出力
^^^^^^

.. highlight:: none

::

  #include <stdio.h>

  int main(int argc, char *argv[])
  {
  	char str[BUFSIZ];

  	printf("> ");
  	fgets(str, BUFSIZ, stdin);
  	printf("str= '%s'\n", str);

  	return 0;
  }
    

.. highlight:: none

::

  $ gcc -Wall -Werror -g -o 02_fgets 02_fgets.c
  $ ./02_fgets
  > hello,world
  str= 'hello,world
  '
  $
    

データ型
^^^^^^^^

.. highlight:: none

::

  #include <stdio.h>

  int main(int argc, char *argv[])
  {
  	printf("sizeof(char)= %d\n", sizeof(char));
  	printf("sizeof(char *)= %d\n", sizeof(char *));
  	printf("sizeof(int)= %d\n", sizeof(int));
  	printf("sizeof(int *)= %d\n", sizeof(int *));
  	printf("sizeof(long)= %d\n", sizeof(long));
  	printf("sizeof(long *)= %d\n", sizeof(long *));
  	printf("sizeof(float)= %d\n", sizeof(float));
  	printf("sizeof(float *)= %d\n", sizeof(float *));
  	printf("sizeof(double)= %d\n", sizeof(double));
  	printf("sizeof(double *)= %d\n", sizeof(double *));

  	return 0;
  }
    
.. highlight:: none

::

  $ gcc -Wall -Werror -g -o 03_data_type 03_data_type.c
  $ ./03_data_type
  sizeof(char)= 1
  sizeof(char *)= 4
  sizeof(int)= 4
  sizeof(int *)= 4
  sizeof(long)= 4
  sizeof(long *)= 4
  sizeof(float)= 4
  sizeof(float *)= 4
  sizeof(double)= 8
  sizeof(double *)= 4
    

制御構文
^^^^^^^^

for文による繰り返し
~~~~~~~~~~~~~~~~~~~

.. highlight:: none

::

  #include <stdio.h>

  int main(int argc, char *argv[])
  {
  	int i;

  	for (i = 0; i < 10; i++) {
  		printf("%dべえ\n", i);
  	}

  	return 0;
  }
    

.. highlight:: none

::

  $ gcc -Wall -Werror -g -o 04_iterate_for 04_iterate_for.c
  $ ./04_iterate_for
  0べえ
  1べえ
  2べえ
  3べえ
  4べえ
  5べえ
  6べえ
  7べえ
  8べえ
  9べえ
    

.. highlight:: none

::

  #include <stdio.h>

  int main(int argc, char *argv[])
  {
  	char str[BUFSIZ];

  	printf("Ctrl-D to quit > ");
  	while (fgets(str, BUFSIZ, stdin) != NULL) {
  		printf("str= '%s'\n", str);
  		printf("Ctrl-D to quit > ");
  	}

  	return 0;
  }
    

.. highlight:: none

::

  $ gcc -Wall -Werror -g -o 04_iterate_while 04_iterate_while.c
  $ ./04_iterate_while
  Ctrl-D to quit > hello
  str= 'hello
  '
  Ctrl-D to quit > world
  str= 'world
  '
  Ctrl-D to quit > (Ctrl-Dを入力する)
  $
    

関数と戻り値
^^^^^^^^^^^^

.. highlight:: none

::

  #include <stdio.h>
  #include <stdlib.h> /* atoi() */

  int add(int x, int y)
  {
  	return x + y;
  }

  int main(int argc, char *argv[])
  {
  	int x, y, sum;
  	char buf[BUFSIZ];

  	printf("x= ");
  	fgets(buf, BUFSIZ, stdin);
  	x = atoi(buf);

  	printf("y= ");
  	fgets(buf, BUFSIZ, stdin);
  	y = atoi(buf);

  	sum = add(x, y);
  	printf("sum= %d\n", sum);

  	return 0;
  }
    

.. highlight:: none

::

  $ gcc -Wall -Werror -g -o 05_function 05_function.c
  $ ./05_function
  x= 5
  y= 7
  sum= 12
    

変数のスコープ
^^^^^^^^^^^^^^

.. highlight:: none

::

  #include <stdio.h>
  
  int global_val1 = 10;
  int global_val2 = 11;
  
  int func(void)
  {
  	int local_val = 20;
  	int global_val2 = 21;

  	printf("in func()\n");
  	printf("local_val  = %d\n", local_val);
  	printf("global_val1= %d\n", global_val1);
  	printf("global_val2= %d\n", global_val2);

  	global_val1 = 12;

  	return 0;
  }
      
  int main(int argc, char *argv[])
  {
  	int local_val = 30;
  
  	printf("before func()\n");
  	printf("local_val  = %d\n", local_val);
  	printf("global_val1= %d\n", global_val1);
  	printf("global_val2= %d\n", global_val2);
  
  	func();
  
  	printf("after func()\n");
  	printf("local_val  = %d\n", local_val);
  	printf("global_val1= %d\n", global_val1);
  	printf("global_val2= %d\n", global_val2);
  
  	return 0;
  }
  

.. highlight:: none

::

  $ gcc -Wall -Werror -g -o 06_variable_scope 06_variable_scope.c
  $ ./06_variable_scope
  before func()
  local_val  = 30
  global_val1= 10
  global_val2= 11
  in func()
  local_val  = 20
  global_val1= 10
  global_val2= 21
  after func()
  local_val  = 30
  global_val1= 12
  global_val2= 11
    
staticな変数
^^^^^^^^^^^^

.. highlight:: none

::

  #include <stdio.h>
  
  int main(int argc, char *argv[])
  {
  	int variable;
  	static int static_variable;
  
  	printf("variable       = %d\n", variable);
  	printf("static_variable= %d\n", static_variable);
  
  	return 0;
  }
    

.. highlight:: none

::

  $ gcc -Wall -g -o 07_static_variable 07_static_variable.c
  07_static_variable.c: In function ‘main’:
  07_static_variable.c:8: 警告: ‘variable’ is used uninitialized in this function
  $ ./07_static_variable
  variable       = 6905844
  static_variable= 0
    

extern宣言
^^^^^^^^^^

.. highlight:: none

::

  int extern_global_value = 123;


.. highlight:: none

::

  #include <stdio.h>

  extern int extern_global_value;

  int main(int argc, char *argv[])
  {
  	printf("%d\n", extern_global_value);
  	return 0;
  }

    
.. highlight:: none

::

  $ gcc -Wall -Werror -g -o 08_main 08_main.c 08_variable_decl.c
  $ ./08_main
  123


ヘッダファイル
^^^^^^^^^^^^^^

インクルードファイル
^^^^^^^^^^^^^^^^^^^^

ポインタ
^^^^^^^^

.. highlight:: none

::

  #include <stdio.h>

  int main(int argc, char *argv[])
  {
  	char str[] = "HELLO";
  	char *s = str;

  	for (s = str; *s != '\0'; s++) {
  		printf("'%s'\n", s);
  	}

  	return 0;
  }
    

.. highlight:: none

::

  $ gcc -Wall -Werror -g -o 09_pointer 09_pointer.c
  $ ./09_pointer
  'HELLO'
  'ELLO'
  'LLO'
  'LO'
  'O'
    

文字列
^^^^^^

.. highlight:: none

::

  #include <stdio.h>

  int main(int argc, char *argv[])
  {
  	int i, j;
  	char *c;

  	for (i = 0; i < argc; i++) {
  		c = argv[i];
  		for (j = 0; *c != '\0'; c++) {
  			printf("'%c' ", *c);
  		}
  		printf("\n");
  	}

  	return 0;
  }
    

.. highlight:: none

::

  $ gcc -Wall -Werror -g -o 10_string 10_string.c
  $ ./10_string hello world
  '.' '/' '1' '0' '_' 's' 't' 'r' 'i' 'n' 'g'
  'h' 'e' 'l' 'l' 'o'
  'w' 'o' 'r' 'l' 'd'
    

make
----

.. highlight:: none

::

  # Makefile

  BIN	= 08_main
  SRC	= $(BIN).c 08_variable_decl.c
  CC	= gcc
  DEBUG	= -Wall -Werror -g

  CFLAGS	=
  LIBS	=

  all: $(SRC)
  	$(CC) $(DEBUG) -o $(BIN) $(SRC) $(CFLAGS) $(LIBS)

  clean:
  	rm $(BIN)

  rebuild:
  	make clean
  	make all
    

binutilsを使ってみる
--------------------

.. highlight:: none

::

  #include <stdio.h>
  #include <stdlib.h> /* atoi() */

  int func(int x)
  {
  	int sum, i;

  	if (x <= 0) {
  		return 0;
  	}

  	sum = 0;
  	for (i = 1; i <= x; i++) {
  		sum += i;
  	}

  	return sum;
  }

  int main(int argc, char *argv[])
  {
  	int sum, x = 10;

  	if (argc > 1) {
  		x = atoi(argv[1]);
  	}

  	sum = func(x);
  	printf("sum= %d\n", sum);
  	
  	return 0;
  }
    

.. highlight:: none

::

  $ gcc -Wall -Werror -g -o 01_sample 01_sample.c
  $ ./01_sample
  sum= 55

実行ファイル中のシンボルを確認する
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. highlight:: none

::

  $ nm ./01_sample
  08049614 d _DYNAMIC
  080496e0 d _GLOBAL_OFFSET_TABLE_
  0804854c R _IO_stdin_used
           w _Jv_RegisterClasses
  08049604 d __CTOR_END__
  08049600 d __CTOR_LIST__
  0804960c D __DTOR_END__
  08049608 d __DTOR_LIST__
  080485fc r __FRAME_END__
  08049610 d __JCR_END__
  08049610 d __JCR_LIST__
  08049700 A __bss_start
  080496fc D __data_start
  08048500 t __do_global_ctors_aux
  08048370 t __do_global_dtors_aux
  08048550 R __dso_handle
           w __gmon_start__
  080484fa T __i686.get_pc_thunk.bx
  08049600 d __init_array_end
  08049600 d __init_array_start
  08048490 T __libc_csu_fini
  080484a0 T __libc_csu_init
           U __libc_start_main@@GLIBC_2.0
  08049700 A _edata
  08049708 A _end
  0804852c T _fini
  08048548 R _fp_hw
  080482b4 T _init
  08048340 T _start
           U atoi@@GLIBC_2.0
  08049700 b completed.5972
  080496fc W data_start
  08049704 b dtor_idx.5974
  080483d0 t frame_dummy
  080483f4 T func
  0804842e T main
           U printf@@GLIBC_2.0
    
実行ファイルを逆アセンブルする
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. highlight:: none

::

  $ objdump -d 01_sample

  01_sample:     file format elf32-i386
  ...中略...
  080483f4 <func>:
   80483f4:       55                      push   %ebp
   80483f5:       89 e5                   mov    %esp,%ebp
   80483f7:       83 ec 10                sub    $0x10,%esp
   80483fa:       83 7d 08 00             cmpl   $0x0,0x8(%ebp)
   80483fe:       7f 07                   jg     8048407 <func+0x13>
   8048400:       b8 00 00 00 00          mov    $0x0,%eax
   8048405:       eb 25                   jmp    804842c <func+0x38>
   8048407:       c7 45 f8 00 00 00 00    movl   $0x0,-0x8(%ebp)
   804840e:       c7 45 fc 01 00 00 00    movl   $0x1,-0x4(%ebp)
   8048415:       eb 0a                   jmp    8048421 <func+0x2d>
   8048417:       8b 45 fc                mov    -0x4(%ebp),%eax
   804841a:       01 45 f8                add    %eax,-0x8(%ebp)
   804841d:       83 45 fc 01             addl   $0x1,-0x4(%ebp)
   8048421:       8b 45 fc                mov    -0x4(%ebp),%eax
   8048424:       3b 45 08                cmp    0x8(%ebp),%eax
   8048427:       7e ee                   jle    8048417 <func+0x23>
   8048429:       8b 45 f8                mov    -0x8(%ebp),%eax
   804842c:       c9                      leave
   804842d:       c3                      ret

  0804842e <main>:
   804842e:       55                      push   %ebp
   804842f:       89 e5                   mov    %esp,%ebp
   8048431:       83 e4 f0                and    $0xfffffff0,%esp
   8048434:       83 ec 20                sub    $0x20,%esp
   8048437:       c7 44 24 1c 0a 00 00    movl   $0xa,0x1c(%esp)
   804843e:       00
   804843f:       83 7d 08 01             cmpl   $0x1,0x8(%ebp)
   8048443:       7e 14                   jle    8048459 <main+0x2b>
   8048445:       8b 45 0c                mov    0xc(%ebp),%eax
   8048448:       83 c0 04                add    $0x4,%eax
   804844b:       8b 00                   mov    (%eax),%eax
   804844d:       89 04 24                mov    %eax,(%esp)
   8048450:       e8 cf fe ff ff          call   8048324 <atoi@plt>
   8048455:       89 44 24 1c             mov    %eax,0x1c(%esp)
   8048459:       8b 44 24 1c             mov    0x1c(%esp),%eax
   804845d:       89 04 24                mov    %eax,(%esp)
   8048460:       e8 8f ff ff ff          call   80483f4 <func>
   8048465:       89 44 24 18             mov    %eax,0x18(%esp)
   8048469:       b8 54 85 04 08          mov    $0x8048554,%eax
   804846e:       8b 54 24 18             mov    0x18(%esp),%edx
   8048472:       89 54 24 04             mov    %edx,0x4(%esp)
   8048476:       89 04 24                mov    %eax,(%esp)
   8048479:       e8 96 fe ff ff          call   8048314 <printf@plt>
   804847e:       b8 00 00 00 00          mov    $0x0,%eax
   8048483:       c9                      leave
   8048484:       c3                      ret
   8048485:       90                      nop
   8048486:       90                      nop
   8048487:       90                      nop
   8048488:       90                      nop
   8048489:       90                      nop
   804848a:       90                      nop
   804848b:       90                      nop
   804848c:       90                      nop
   804848d:       90                      nop
   804848e:       90                      nop
   804848f:       90                      nop
    

実行ファイルの情報を確認する
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. highlight:: none

::

  $ readelf -a 01_sample
  ELF Header:
    Magic:   7f 45 4c 46 01 01 01 00 00 00 00 00 00 00 00 00
    Class:                             ELF32
    Data:                              2's complement, little endian
    Version:                           1 (current)
    OS/ABI:                            UNIX - System V
    ABI Version:                       0
    Type:                              EXEC (Executable file)
    Machine:                           Intel 80386
    Version:                           0x1
    Entry point address:               0x8048340
    Start of program headers:          52 (bytes into file)
    Start of section headers:          3028 (bytes into file)
    Flags:                             0x0
    Size of this header:               52 (bytes)
    Size of program headers:           32 (bytes)
    Number of program headers:         8
    Size of section headers:           40 (bytes)
    Number of section headers:         38
    Section header string table index: 35

  Section Headers:
  ...略...
    

アセンブリ言語で関数を作ってみる
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. highlight:: none

::

  	.text
  .globl	func
  func:
  	pushl	%ebp
  	movl	%esp,%ebp
  	movl	$0x62a4,%eax
  	leave
  	ret
    

.. highlight:: none

::

  #include <stdio.h>

  int func(void);

  int main(int argc, char *argv[])
  {
  	int x;

  	x = func();
  	printf("x= %d\n", x);

  	return 0;
  }
    

.. highlight:: none

::

  # Makefile

  BIN	= 02_main
  SRC	= $(BIN).c 02_func.s
  CC	= gcc
  DEBUG	= -Wall -Werror -g

  CFLAGS	=
  LIBS	=

  all: $(SRC)
  	$(CC) $(DEBUG) -o $(BIN) $(SRC) $(CFLAGS) $(LIBS)

  clean:
  	rm $(BIN)

  rebuild:
  	make clean
  	make all

    
.. highlight:: none

::

  $ make
  gcc -Wall -Werror -g -o 02_main 02_main.c 02_func.s
  $ ./02_main
  x= 25252


デバッガ
--------

デバッガ上で実行ファイルを走らせる
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. highlight:: none

::

  $ gdb -q ./01_sample
  Reading symbols from /home/foo/bar/02/binutils/01_sample...done.
  (gdb) run
  Starting program: /home/foo/bar/02/binutils/01_sample
  sum= 55

  Program exited normally.
  Missing separate debuginfos, use: debuginfo-install glibc-2.12-1.80.el6_3.5.i686
  (gdb)
    

ブレークポイントを設定する
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. highlight:: none

::

  (gdb) break func
  Breakpoint 1 at 0x80483fa: file 01_sample.c, line 8.
  (gdb) info break
  Num     Type           Disp Enb Address    What
  1       breakpoint     keep y   0x080483fa in func at 01_sample.c:8
          breakpoint already hit 1 time
  (gdb) run
  Starting program: /home/fpig/yry2_r/work/02/binutils/01_sample

  Breakpoint 1, func (x=10) at 01_sample.c:8
  8               if (x <= 0) {
  (gdb) continue
  Continuing.
  sum= 55

  Program exited normally.

  (gdb) del 1
  (gdb) info break
  No breakpoints or watchpoints.
  (gdb)
    

変数を確認する
^^^^^^^^^^^^^^

.. highlight:: none

::

  (gdb) run
  Breakpoint 2, func (x=10) at 01_sample.c:8
  8               if (x <= 0) {
  (gdb) list
  3
  4       int func(int x)
  5       {
  6               int sum, i;
  7
  8               if (x <= 0) {
  9                       return 0;
  10              }
  11
  12              sum = 0;
  (gdb) p x
  $3 = 10
  (gdb)

    
ステップ実行する
^^^^^^^^^^^^^^^^

.. highlight:: none

::

  (gdb) run
  Breakpoint 2, func (x=10) at 01_sample.c:8
  8               if (x <= 0) {
  (gdb) run
  The program being debugged has been started already.
  Start it from the beginning? (y or n) y

  Starting program: /home/fpig/yry2_r/work/02/binutils/01_sample

  Breakpoint 2, func (x=10) at 01_sample.c:8
  8               if (x <= 0) {
  (gdb) step
  12              sum = 0;
  (gdb)
  13              for (i = 1; i <= x; i++) {
  (gdb)
  14                      sum += i;
  (gdb)
  13              for (i = 1; i <= x; i++) {
  (gdb)
  14                      sum += i;
  (gdb)
  13              for (i = 1; i <= x; i++) {
  (gdb)
  13              for (i = 1; i <= x; i++) {
  (gdb)
  14                      sum += i;
  (gdb)
  13              for (i = 1; i <= x; i++) {
  (gdb)

    
マシン語1命令毎にステップ実行する
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


デバッガから関数を呼んでみる
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. highlight:: none

::

  (gdb) break main
  Breakpoint 3 at 0x8048437: file 01_sample.c, line 22.
  (gdb) run
  Starting program: /home/fpig/yry2_r/work/02/binutils/01_sample

  Breakpoint 3, main (argc=1, argv=0xbffff6b4) at 01_sample.c:22
  22              int sum, x = 10;
  (gdb) call printf("hello,world.\n")
  hello,world.
  $4 = 13
  (gdb)

    
ブレークポイントに紐づけて処理を実行する
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. highlight:: none

::

  $ gdb -q ./01_sample
  Reading symbols from /home/foo/bar/01_sample...done.
  (gdb) break func
  Breakpoint 1 at 0x80483fa: file 01_sample.c, line 8.
  (gdb) command
  Type commands for breakpoint(s) 1, one per line.
  End with a line saying just "end".
  >call printf("hello,world.\n")
  >continue
  >end
  (gdb) run
  Starting program: /home/fpig/yry2_r/work/02/binutils/01_sample

  Breakpoint 1, func (x=10) at 01_sample.c:8
  8               if (x <= 0) {
  hello,world.
  $1 = 13
  sum= 55

  Program exited normally.
  Missing separate debuginfos, use: debuginfo-install glibc-2.12-1.80.el6_3.5.i686
  (gdb)

    
なんちゃってライブパッチを行う
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. highlight:: none

::

  #include <stdio.h>
  #include <string.h> /* strncpy() */
  #include <stdlib.h> /* malloc() */

  int main(int argc, char *argv[])
  {
  	char *str = NULL;

  	*str = '@';

  	printf("%s\n", str);
  	return 0;
  }
    

.. highlight:: none

::

  $ gcc -Wall -Werror -g -o 01_sample2 01_sample2.c
  $ ./01_sample2
  セグメンテーション違反です


.. highlight:: none

::

  $ gdb -q ./01_sample2
  Reading symbols from /home/foo/bar/01_sample2...done.
  (gdb) run
  Starting program: /home/foo/bar/01_sample2

  Program received signal SIGSEGV, Segmentation fault.
  0x080483c9 in main (argc=1, argv=0xbffff6b4) at 01_sample2.c:9
  9               *str = '@';
  Missing separate debuginfos, use: debuginfo-install glibc-2.12-1.80.el6_3.5.i686
  (gdb) bt
  #0  0x080483c9 in main (argc=1, argv=0xbffff6b4) at 01_sample2.c:9
  (gdb) list
  4
  5       int main(int argc, char *argv[])
  6       {
  7               char *str = NULL;
  8
  9               *str = '@';
  10
  11              printf("%s\n", str);
  12              return 0;
  13      }
  (gdb)
    

.. highlight:: none

::

  set height 0

  file 01_sample2

  break 01_sample2.c:9
  command
  	set $mem = malloc(32)
  	call strncpy($mem, "HELLO,WORLD.", 31)
  	call str = $mem
  	next
  	continue
  end

    
.. highlight:: none

::

  $ gdb -q -x 01_live_patch.gdb
  Breakpoint 1 at 0x80483c5: file 01_sample2.c, line 9.
  (gdb) run
  Starting program: /home/foo/bar/01_sample2

  Breakpoint 1, main (argc=1, argv=0xbffff6b4) at 01_sample2.c:9
  9               *str = '@';
  $1 = 134520840
  $2 = 0x804a008 "HELLO,WORLD."
  11              printf("%s\n", str);
  Missing separate debuginfos, use: debuginfo-install glibc-2.12-1.80.el6_3.5.i686
  (gdb) c
  Continuing.
  @ELLO,WORLD.

  Program exited normally.
  (gdb) q

    
デバッガ中で変数を使う
^^^^^^^^^^^^^^^^^^^^^^

.. highlight:: none

::

  (gdb) set $mem = malloc(32)
  (gdb) call strncpy($mem, "hello,world.", 31)
  $1 = 134520840
  (gdb) p $mem
  $2 = 134520840
  (gdb) call printf("%s\n", $mem)
  hello,world.
  $3 = 13
  (gdb) set $i = 0
  (gdb) call printf("[%d] %c\n", $i, *($mem +$i++))
  [0] h
  $6 = 6
  (gdb)
  [1] e
  $7 = 6
  (gdb)
  [2] l
  $8 = 6
  (gdb)
  [3] l
  $9 = 6
  (gdb)
  [4] o
  $10 = 6
  (gdb)

    
coreファイルからデバッグする
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

その他
^^^^^^

gdbの機能を使ってユニットテストを行う
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

関数のコールグラフを作ってみる
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

C言語プログラミングから見るUNIXの機能
-------------------------------------

標準入力/出力/エラー出力
^^^^^^^^^^^^^^^^^^^^^^^^

ファイル操作
^^^^^^^^^^^^

プロセス生成
^^^^^^^^^^^^

シグナル
^^^^^^^^

IO多重化(poll/select)
^^^^^^^^^^^^^^^^^^^^^

名前付きパイプ
^^^^^^^^^^^^^^

ソケット
^^^^^^^^

シェルスクリプト
----------------
